include $(TOPDIR)/config.mk

%.build:
	$(MAKE) -C $* DESTDIR=$(CURDIR)/$*.dest
	cd $*.dest && $(TOPDIR)/makepkg -l y -c n $(TOPDIR)/$*.tgz

%.install: %.build
	test -n "$(DESTDIR)"
	./installpkg --root "$(DESTDIR)" $*.tgz

%.PATH:
	command -v $* || $(MAKE) $*.install DESTDIR=$(BROOT)
