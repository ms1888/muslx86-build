export TOPDIR = $(CURDIR)

include common.mk

all: muslx86.build muslx86-headers.build

install: muslx86.install muslx86-headers.install
